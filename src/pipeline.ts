import { Conf } from "$src/config.ts";
import { DB } from "$src/db.ts";
import { LRUCache, NIP05, NostrEvent, verifyEvent } from "$src/deps.ts";
import { Client } from "$src/firehose.ts";
import { Queries } from "$src/queries.ts";

export class Pipeline {
  static encounters = new LRUCache<string, boolean>({ max: 1000 });

  static async event(event: NostrEvent): Promise<void> {
    if (Pipeline.encounters.get(event.id)) {
      return;
    } else {
      Pipeline.encounters.set(event.id, true);
    }

    console.debug(`Event<${event.kind}>: ${event.id}`);
    const admin = await Conf.signer.getPublicKey();

    if (event.kind !== 5950 && event.pubkey !== admin) {
      throw new Error("Only NIP-05 job requests are permitted");
    }
    if (!verifyEvent(event)) {
      throw new Error("Invalid event `id` or `sig`");
    }

    const db = await DB.getInstance();
    await db.event(event);

    if (event.kind === 5950) {
      await Pipeline.job(event);
    }

    if (event.pubkey === admin) {
      const client = Client.getInstance();
      await client.event(event);
    }
  }

  static async job(event: NostrEvent): Promise<void> {
    const { signer, domains } = Conf;

    const admin = await signer.getPublicKey();
    const input = event.tags.find(([name]) => name === "i")?.[1];

    const tagged = !!event.tags.find(([name, value]) =>
      name === "p" && value === admin
    );

    if (!input || !NIP05.regex().test(input)) {
      return Pipeline.feedback(event, "error", `Invalid name: ${input}`);
    }

    const [user, host] = input.split("@");
    const nip05 = `${user}@${host}`;

    if (!domains.includes(host) && tagged) {
      return Pipeline.feedback(event, "error", `Unsupported domain: ${host}`);
    }

    if (user === "_") {
      return Pipeline.feedback(event, "error", `Forbidden user: ${user}`);
    }

    const db = await DB.getInstance();
    const taken = await Queries.getNip05Label(db, nip05);

    if (taken) {
      return Pipeline.feedback(event, "error", `Name already taken: ${nip05}`);
    }

    await Pipeline.label(nip05, event.pubkey);
    await Pipeline.result(event, nip05);
  }

  static async feedback(
    event: NostrEvent,
    status: "payment-required" | "processing" | "error" | "success" | "partial",
    info = "",
  ): Promise<void> {
    const feedback = await Conf.signer.signEvent({
      kind: 7000,
      content: "",
      tags: [
        ["status", status, info],
        ["e", event.id],
        ["p", event.pubkey],
      ],
      created_at: Math.floor(Date.now() / 1000),
    });
    return Pipeline.event(feedback);
  }

  static async label(nip05: string, pubkey: string): Promise<void> {
    const label = await Conf.signer.signEvent({
      kind: 1985,
      tags: [
        ["L", "nip05"],
        ["l", nip05],
        ["p", pubkey],
      ],
      content: "",
      created_at: Math.floor(Date.now() / 1000),
    });
    return Pipeline.event(label);
  }

  static async result(event: NostrEvent, nip05: string): Promise<void> {
    const result = await Conf.signer.signEvent({
      kind: 6950,
      content: nip05,
      tags: [
        ["request", JSON.stringify(event)],
        ["i", nip05, "text"],
        ["e", event.id],
        ["p", event.pubkey],
      ],
      created_at: Math.floor(Date.now() / 1000),
    });
    return Pipeline.event(result);
  }
}
