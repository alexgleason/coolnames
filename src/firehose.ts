import { NostrEvent, NostrFilter, NRelay1, NSet } from "$src/deps.ts";
import { Pipeline } from "$src/pipeline.ts";
import { Conf } from "$src/config.ts";

export class Firehose {
  relays = new Map<WebSocket["url"], NRelay1>();

  constructor(relayUrls: string[]) {
    for (const url of relayUrls) {
      const relay = new NRelay1(url);
      this.relays.set(relay.socket.url, relay);
      Firehose.subscribe(relay).catch(console.warn);
    }
    console.log(`Firehose: ${relayUrls.join(", ")}`);
  }

  private static async subscribe(relay: NRelay1) {
    for await (const msg of relay.req([{ kinds: [5950], limit: 0 }])) {
      if (msg[0] === "EVENT") {
        Pipeline.event(msg[2]).catch(console.warn);
      } else {
        console.log(msg);
      }
    }
  }

  async event(event: NostrEvent): Promise<void> {
    for (const relay of this.relays.values()) {
      await relay.event(event).catch(console.warn);
    }
  }

  async query(
    filters: NostrFilter[],
    opts?: { signal?: AbortSignal },
  ): Promise<NostrEvent[]> {
    const nset = new NSet();
    const promises: Promise<NostrEvent[]>[] = [];

    for (const relay of this.relays.values()) {
      promises.push(relay.query(filters, opts).catch(() => []));
    }
    for (const event of (await Promise.all(promises)).flat()) {
      nset.add(event);
    }

    return [...nset];
  }
}

export class Client {
  static firehose: Firehose;

  static getInstance() {
    if (!Client.firehose) {
      const relays = Conf.relays;
      if (!relays.length) {
        console.warn("No relays found in environment variable RELAYS");
      }
      Client.firehose = new Firehose(relays);
    }
    return Client.firehose;
  }
}
