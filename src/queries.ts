import { Conf } from "$src/config.ts";
import { NostrEvent, NStore } from "$src/deps.ts";

export class Queries {
  static async getNip05Label(
    storage: NStore,
    nip05: string,
  ): Promise<NostrEvent | undefined> {
    const admin = await Conf.signer.getPublicKey();

    const [event] = await storage.query([{
      kinds: [1985],
      authors: [admin],
      "#L": ["nip05"],
      "#l": [nip05],
    }]);

    return event;
  }
}
