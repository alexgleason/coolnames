import {
  DenoSqliteDialect,
  Kysely,
  NDatabase,
  NKinds,
  NSchema as n,
  Sqlite,
} from "$src/deps.ts";

function isNostrId(value: string): boolean {
  return n.id().safeParse(value).success;
}

export class DB {
  static #db: NDatabase;

  static async getInstance() {
    if (!DB.#db) {
      const kysely = new Kysely({
        dialect: new DenoSqliteDialect({
          database: new Sqlite("./db.sqlite3"),
        }),
      });
      DB.#db = new NDatabase(
        kysely,
        {
          tagIndexes: {
            "d": ({ event, count }) =>
              count === 0 && NKinds.parameterizedReplaceable(event.kind),
            "e": ({ count, value }) => (count < 15) && isNostrId(value),
            "L": ({ event, count }) => event.kind === 1985 || count === 0,
            "l": ({ event, count }) => event.kind === 1985 || count === 0,
            "P": ({ event, count, value }) =>
              event.kind === 9735 && count === 0 && isNostrId(value),
            "p": ({ event, count, value }) =>
              (count < 15 || event.kind === 3) && isNostrId(value),
            "q": ({ event, count, value }) =>
              count === 0 && event.kind === 1 && isNostrId(value),
            "t": ({ count, value }) => count < 5 && value.length < 50,
          },
        },
      );
      await DB.#db.migrate();
    }
    return DB.#db;
  }
}
