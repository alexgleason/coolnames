import { nip19, NostrSigner, NSecSigner } from "$src/deps.ts";

export class Conf {
  static get secretKey(): Uint8Array {
    const nsec = Deno.env.get("NSEC");
    if (!nsec) {
      throw new Error("NSEC environment variable is not set");
    }
    const result = nip19.decode(nsec);
    if (result.type !== "nsec") {
      throw new Error("Invalid NSEC environment variable");
    }
    return result.data;
  }

  static get signer(): NostrSigner {
    return new NSecSigner(this.secretKey);
  }

  static get relays(): WebSocket["url"][] {
    const relays = Deno.env.get("RELAYS") ?? "";
    return relays.split(",").map((str) => str.trim()).filter(Boolean);
  }

  static get domains(): URL["hostname"][] {
    const domains = Deno.env.get("DOMAINS") ?? "";
    return domains.split(",").map((str) => str.trim()).filter(Boolean);
  }
}
