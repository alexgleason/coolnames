import { DB } from "$src/db.ts";
import { Queries } from "$src/queries.ts";

export async function handler(req: Request): Promise<Response> {
  const db = await DB.getInstance();

  const { host, searchParams } = new URL(req.url);
  const name = searchParams.get("name");

  if (!name) {
    return new Response("Bad request", { status: 400 });
  }

  console.log(`${name}@${host}`);
  const event = await Queries.getNip05Label(db, `${name}@${host}`);
  const [_p, pubkey, relay] = event?.tags?.find(([name]) => name === "p") ?? [];

  if (!pubkey) {
    return new Response("Not found", { status: 404 });
  }

  const result = {
    names: { [name]: pubkey },
    relays: relay ? { [pubkey]: [relay] } : undefined,
  };

  return new Response(JSON.stringify(result));
}
